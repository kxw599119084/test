package com.mumu233.test.provider;

import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.mapping.MappedStatement;
import tk.mybatis.mapper.mapperhelper.*;

/**
 * @author mumu
 * @date 2019/09/12 上午10:33
 **/
public class MySelectProvider extends MapperTemplate {


    public MySelectProvider(Class<?> mapperClass, MapperHelper mapperHelper) {
        super(mapperClass, mapperHelper);
    }

    public String selectOneByChange(MappedStatement ms){
        Class<?> entityClass = this.getEntityClass(ms);
        StringBuilder sql = new StringBuilder();
        sql.append("select concat('$',id)");
        sql.append(SqlHelper.fromTable(entityClass,this.tableName(entityClass)));
        sql.append("where id = 120");
        return sql.toString();
    }
}
