package com.mumu233.test;
import	java.util.Optional;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author mumu
 * @date 2019/08/29 下午5:14
 * 利用模板生成PDF
 **/
public class PDFBuder {

    /**
     * 输出路径 请包含文件名 不能包含中文路径
     */
    private String outUrl;

    /**
     * 模板名字 templates下
     */
    private String templateName;

    public PDFBuder(String outUrl, String templateName) {
        this.outUrl = outUrl;
        this.templateName = templateName;
    }

    /**
     * 生成文件
     * @return  输入路径
     * @param reportFTLEntity  生成用实体
     */
    public String start(ReportFTLEntity reportFTLEntity){
        if(reportFTLEntity == null){

        }

        Map data = new HashMap();
        data.put("data",reportFTLEntity);

        Configuration configuration = new Configuration();
        configuration.setClassForTemplateLoading(this.getClass(),"/templates");
        configuration.setDefaultEncoding("utf-8");

        Template template = null;

        try {
            template = configuration.getTemplate("template.ftl");
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(template == null){

        }

        File outFile = new File(outUrl);
        Writer out = null;

        try {
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "utf-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            template.process(data, out);
        } catch (TemplateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }
        return outUrl;
    }
}
