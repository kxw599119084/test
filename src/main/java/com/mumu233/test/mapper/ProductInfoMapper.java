package com.mumu233.test.mapper;

import com.mumu233.test.ProductInfoEntity;
import com.mumu233.test.mapper.base.SelectMapper;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.BaseMapper;

/**
 * @author mumu
 * @date 2019/09/10 上午9:44
 **/
@org.apache.ibatis.annotations.Mapper
@Repository
public interface ProductInfoMapper extends BaseMapper<ProductInfoEntity> {

}
