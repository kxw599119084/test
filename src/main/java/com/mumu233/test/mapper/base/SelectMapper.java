package com.mumu233.test.mapper.base;

import com.mumu233.test.provider.MySelectProvider;
import org.apache.ibatis.annotations.SelectProvider;
import tk.mybatis.mapper.annotation.RegisterMapper;


/**
 * @author mumu
 * @date 2019/09/12 上午11:11
 **/
@RegisterMapper
public interface SelectMapper<T> {

    @SelectProvider(
            type = MySelectProvider.class,
            method = "selectOneByChange"
    )
    String selectOneByChange();
}
