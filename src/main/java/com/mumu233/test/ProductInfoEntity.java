package com.mumu233.test;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
* @author mumu
* @date 2019-08-21 11:42:16
*/
@Data
@Table(name = "product_info")
public class ProductInfoEntity implements Serializable {
    private static final long serialVersionUID = 755613562932446124L;
    

        @Id
        @Column(name = "id")
        private Integer id;

        private String merchantCode;

        private String productCode;

        private String productProfileUrl;

        private String productStatus;

        private String productCat;

        private String productCatNm;

        private String productProperty;

        private String productUrl;

        private BigDecimal productOrgPrice;

        private String productType;

        private String productSubCode;

        private BigDecimal productPrice;

        private String productDemoTime;

        private String productSection;

        private String productDesc;

        private BigDecimal expressPrice;

        private String productTags;

        private String productSpecification;

        private Date publishDate;

        private String productWxProfileUrl;

        private String productTitle;

        private Integer productNum;

        private Integer saleCount;

        private Integer commentTimes;

        private BigDecimal productScore;

        private Integer sortNo;

        private Long createBy;

        private Date createDate;

        private Date updateDate;

        private String delFlag;

        private String productSectionOriginal;

        private String productRecommendCode;

        private Integer lookCount;

        private Integer status;

        private String goodslistv2;

}