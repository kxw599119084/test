package com.mumu233.test;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author mumu
 * @date 2019/08/29 下午2:40
 **/

@NoArgsConstructor
@Data
public class ReportFTLEntity {


    /**
     * pageHeader : {"institutionName":"","title":""}
     * header : {"reportName":"","buyerName":"","address":"","YMD":""}
     * body : {"buyerName":"","points":[{"pointName":"","pointList":[{"riskIndex":1,"riskInedexContent":"","riskLevel":"","riskResult":"","riskLaw":"","riskControl":""}]}]}
     * conclusion :
     * programs : [{"programIndex":"","programTitle":"","programContent":"","programUrl":""}]
     * pageFooter : {"institutionName":"","YMD":"","website":"","publicNumber":"","APPDownload":"","emial":"","address":"","code":""}
     */

    private PageHeader pageHeader;
    private Header header;
    private Body body;
    private String conclusion;
    private PageFooter pageFooter;
    private List<Programs> programs;

    @NoArgsConstructor
    @Data
    public static class PageHeader {
        /**
         * institutionName :
         * title :
         */

        private String institutionName;
        private String title;
    }

    @NoArgsConstructor
    @Data
    public static class Header {
        /**
         * reportName :
         * buyerName :
         * address :
         * YMD :
         */

        private String reportName;
        private String buyerName;
        private String address;
        private String YMD;
    }

    @NoArgsConstructor
    @Data
    public static class Body {
        /**
         * buyerName :
         * points : [{"pointName":"","pointList":[{"riskIndex":1,"riskInedexContent":"","riskLevel":"","riskResult":"","riskLaw":"","riskControl":""}]}]
         */

        private String buyerName;
        private List<Points> points;

        @NoArgsConstructor
        @Data
        public static class Points {
            /**
             * pointName :
             * pointList : [{"riskIndex":1,"riskInedexContent":"","riskLevel":"","riskResult":"","riskLaw":"","riskControl":""}]
             */

            private String pointName;
            private List<PointList> pointList;

            @NoArgsConstructor
            @Data
            public static class PointList {
                /**
                 * riskIndex : 1
                 * riskInedexContent :
                 * riskLevel :
                 * riskResult :
                 * riskLaw :
                 * riskControl :
                 */

                private int riskIndex;
                private String riskInedexContent;
                private String riskLevel;
                private String riskResult;
                private String riskLaw;
                private String riskControl;
            }
        }
    }

    @NoArgsConstructor
    @Data
    public static class PageFooter {
        /**
         * institutionName :
         * YMD :
         * website :
         * publicNumber :
         * APPDownload :
         * emial :
         * address :
         * code :
         */

        private String institutionName;
        private String YMD;
        private String website;
        private String publicNumber;
        private String APPDownload;
        private String emial;
        private String address;
        private String code;
    }

    @NoArgsConstructor
    @Data
    public static class Programs {
        /**
         * programIndex :
         * programTitle :
         * programContent :
         * programUrl :
         */

        private String programIndex;
        private String programTitle;
        private String programContent;
        private String programUrl;
    }
}
