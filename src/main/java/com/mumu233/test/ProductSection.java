package com.mumu233.test;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author mumu
 * @date 2019/09/10 上午9:43
 **/
@NoArgsConstructor
@Data
public class ProductSection {

    /**
     * docNum :
     * docStatus : 1
     * docUrl :
     * productCode :
     * sectionIndex : 1
     * sectionName : 顶级目录
     * txtUrl :
     * videoStatus : 1
     * videoTime :
     * videoUrl :
     * voiceStatus : 1
     * voiceTime :
     * voiceUrl :
     */

    private String docNum;
    private String docStatus;
    private String docUrl;
    private String productCode;
    private String sectionIndex;
    private String sectionName;
    private String txtUrl;
    private String videoStatus;
    private String videoTime;
    private String videoUrl;
    private String voiceStatus;
    private String voiceTime;
    private String voiceUrl;
}
