<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml" xmlns:v="urn:schemas-microsoft-com:vml"
                xmlns:w10="urn:schemas-microsoft-com:office:word"
                xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core"
                xmlns:aml="http://schemas.microsoft.com/aml/2001/core"
                xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"
                xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
                w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no" xml:space="preserve"><o:DocumentProperties>
        <o:Subject>魏镇胜律师向您提示：不要到打官司时才想起律师</o:Subject>
        <o:Author>微软用户</o:Author>
        <o:LastAuthor>yefeng</o:LastAuthor>
        <o:Revision>8</o:Revision>
        <o:LastPrinted>2013-12-12T19:25:00Z</o:LastPrinted>
        <o:Created>2019-05-05T08:26:00Z</o:Created>
        <o:LastSaved>2019-08-29T15:56:05Z</o:LastSaved>
        <o:TotalTime>8640</o:TotalTime>
        <o:Pages>8</o:Pages>
        <o:Words>501</o:Words>
        <o:Characters>2857</o:Characters>
        <o:Company>微软中国</o:Company>
        <o:Lines>23</o:Lines>
        <o:Paragraphs>6</o:Paragraphs>
        <o:CharactersWithSpaces>3352</o:CharactersWithSpaces>
        <o:Version>14</o:Version>
    </o:DocumentProperties>
    <o:CustomDocumentProperties>
        <o:KSOProductBuildVer dt:dt="string">2052-11.1.0.8722</o:KSOProductBuildVer>
    </o:CustomDocumentProperties>
    <w:fonts>
        <w:defaultFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
        <w:font w:name="Times New Roman">
            <w:altName w:val="DejaVu Sans"/>
            <w:panose-1 w:val="00000000000000000000"/>
            <w:charset w:val="00"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="00000000"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="宋体">
            <w:altName w:val="Droid Sans Fallback"/>
            <w:panose-1 w:val="00000000000000000000"/>
            <w:charset w:val="00"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="00000000"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Wingdings">
            <w:panose-1 w:val="05000000000000000000"/>
            <w:charset w:val="02"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="80000000"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Arial">
            <w:altName w:val="DejaVu Sans"/>
            <w:panose-1 w:val="020B0604020202020204"/>
            <w:charset w:val="01"/>
            <w:family w:val="SWiss"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="E0002AFF" w:usb-1="C0007843" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="400001FF"
                   w:csb-1="FFFF0000"/>
        </w:font>
        <w:font w:name="黑体">
            <w:altName w:val="Droid Sans Fallback"/>
            <w:panose-1 w:val="02010609060101010101"/>
            <w:charset w:val="86"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Courier New">
            <w:altName w:val="DejaVu Sans"/>
            <w:panose-1 w:val="02070309020205020404"/>
            <w:charset w:val="01"/>
            <w:family w:val="Modern"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="E0002AFF" w:usb-1="C0007843" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="400001FF"
                   w:csb-1="FFFF0000"/>
        </w:font>
        <w:font w:name="Symbol">
            <w:panose-1 w:val="05050102010706020507"/>
            <w:charset w:val="02"/>
            <w:family w:val="Roman"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="80000000"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Calibri">
            <w:altName w:val="DejaVu Sans"/>
            <w:panose-1 w:val="020F0502020204030204"/>
            <w:charset w:val="00"/>
            <w:family w:val="SWiss"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000001" w:usb-3="00000000" w:csb-0="0000019F"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="微软雅黑">
            <w:altName w:val="Droid Sans Fallback"/>
            <w:panose-1 w:val="020B0503020204020204"/>
            <w:charset w:val="86"/>
            <w:family w:val="SWiss"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="0004001F"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="仿宋_GB2312">
            <w:altName w:val="Droid Sans Fallback"/>
            <w:panose-1 w:val="00000000000000000000"/>
            <w:charset w:val="86"/>
            <w:family w:val="Modern"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Cambria">
            <w:altName w:val="FreeSerif"/>
            <w:panose-1 w:val="02040503050406030204"/>
            <w:charset w:val="00"/>
            <w:family w:val="Roman"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="02000000" w:usb-3="00000000" w:csb-0="0000019F"
                   w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Droid Sans Fallback">
            <w:panose-1 w:val="020B0502000000000001"/>
            <w:charset w:val="86"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="910002FF" w:usb-1="2BDFFCFB" w:usb-2="00000036" w:usb-3="00000000" w:csb-0="203F01FF"
                   w:csb-1="D7FF0000"/>
        </w:font>
        <w:font w:name="aakar">
            <w:panose-1 w:val="02000600040000000000"/>
            <w:charset w:val="00"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="80040001" w:usb-1="00002000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="20000000"
                   w:csb-1="80000000"/>
        </w:font>
        <w:font w:name="FreeSerif">
            <w:panose-1 w:val="02020603050405020304"/>
            <w:charset w:val="00"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="E59FAFFF" w:usb-1="C200FDFF" w:usb-2="43501B29" w:usb-3="04000043" w:csb-0="600101FF"
                   w:csb-1="FFFF0000"/>
        </w:font>
        <w:font w:name="DejaVu Sans">
            <w:panose-1 w:val="020B0603030804020204"/>
            <w:charset w:val="00"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="E7006EFF" w:usb-1="D200FDFF" w:usb-2="0A246029" w:usb-3="0400200C" w:csb-0="600001FF"
                   w:csb-1="DFFF0000"/>
        </w:font>
    </w:fonts>
    <w:styles>
        <w:latentStyles w:defLockedState="off" w:latentStyleCount="260">
            <w:lsdException w:name="Normal"/>
            <w:lsdException
                    w:name="heading 1"/>
            <w:lsdException w:name="heading 2"/>
            <w:lsdException w:name="heading 3"/>
            <w:lsdException
                    w:name="heading 4"/>
            <w:lsdException w:name="heading 5"/>
            <w:lsdException w:name="heading 6"/>
            <w:lsdException
                    w:name="heading 7"/>
            <w:lsdException w:name="heading 8"/>
            <w:lsdException w:name="heading 9"/>
            <w:lsdException
                    w:name="index 1"/>
            <w:lsdException w:name="index 2"/>
            <w:lsdException w:name="index 3"/>
            <w:lsdException
                    w:name="index 4"/>
            <w:lsdException w:name="index 5"/>
            <w:lsdException w:name="index 6"/>
            <w:lsdException
                    w:name="index 7"/>
            <w:lsdException w:name="index 8"/>
            <w:lsdException w:name="index 9"/>
            <w:lsdException
                    w:name="toc 1"/>
            <w:lsdException w:name="toc 2"/>
            <w:lsdException w:name="toc 3"/>
            <w:lsdException
                    w:name="toc 4"/>
            <w:lsdException w:name="toc 5"/>
            <w:lsdException w:name="toc 6"/>
            <w:lsdException
                    w:name="toc 7"/>
            <w:lsdException w:name="toc 8"/>
            <w:lsdException w:name="toc 9"/>
            <w:lsdException
                    w:name="Normal Indent"/>
            <w:lsdException w:name="footnote text"/>
            <w:lsdException w:name="annotation text"/>
            <w:lsdException
                    w:name="header"/>
            <w:lsdException w:name="footer"/>
            <w:lsdException w:name="index heading"/>
            <w:lsdException
                    w:name="caption"/>
            <w:lsdException w:name="table of figures"/>
            <w:lsdException w:name="envelope address"/>
            <w:lsdException
                    w:name="envelope return"/>
            <w:lsdException w:name="footnote reference"/>
            <w:lsdException
                    w:name="annotation reference"/>
            <w:lsdException w:name="line number"/>
            <w:lsdException w:name="page number"/>
            <w:lsdException
                    w:name="endnote reference"/>
            <w:lsdException w:name="endnote text"/>
            <w:lsdException
                    w:name="table of authorities"/>
            <w:lsdException w:name="macro"/>
            <w:lsdException w:name="toa heading"/>
            <w:lsdException
                    w:name="List"/>
            <w:lsdException w:name="List Bullet"/>
            <w:lsdException w:name="List Number"/>
            <w:lsdException
                    w:name="List 2"/>
            <w:lsdException w:name="List 3"/>
            <w:lsdException w:name="List 4"/>
            <w:lsdException
                    w:name="List 5"/>
            <w:lsdException w:name="List Bullet 2"/>
            <w:lsdException w:name="List Bullet 3"/>
            <w:lsdException
                    w:name="List Bullet 4"/>
            <w:lsdException w:name="List Bullet 5"/>
            <w:lsdException w:name="List Number 2"/>
            <w:lsdException
                    w:name="List Number 3"/>
            <w:lsdException w:name="List Number 4"/>
            <w:lsdException w:name="List Number 5"/>
            <w:lsdException
                    w:name="Title"/>
            <w:lsdException w:name="Closing"/>
            <w:lsdException w:name="Signature"/>
            <w:lsdException
                    w:name="Default Paragraph Font"/>
            <w:lsdException w:name="Body Text"/>
            <w:lsdException
                    w:name="Body Text Indent"/>
            <w:lsdException w:name="List Continue"/>
            <w:lsdException
                    w:name="List Continue 2"/>
            <w:lsdException w:name="List Continue 3"/>
            <w:lsdException
                    w:name="List Continue 4"/>
            <w:lsdException w:name="List Continue 5"/>
            <w:lsdException
                    w:name="Message Header"/>
            <w:lsdException w:name="Subtitle"/>
            <w:lsdException w:name="Salutation"/>
            <w:lsdException
                    w:name="Date"/>
            <w:lsdException w:name="Body Text First Indent"/>
            <w:lsdException
                    w:name="Body Text First Indent 2"/>
            <w:lsdException w:name="Note Heading"/>
            <w:lsdException
                    w:name="Body Text 2"/>
            <w:lsdException w:name="Body Text 3"/>
            <w:lsdException w:name="Body Text Indent 2"/>
            <w:lsdException
                    w:name="Body Text Indent 3"/>
            <w:lsdException w:name="Block Text"/>
            <w:lsdException w:name="Hyperlink"/>
            <w:lsdException
                    w:name="FollowedHyperlink"/>
            <w:lsdException w:name="Strong"/>
            <w:lsdException w:name="Emphasis"/>
            <w:lsdException
                    w:name="Document Map"/>
            <w:lsdException w:name="Plain Text"/>
            <w:lsdException w:name="E-mail Signature"/>
            <w:lsdException
                    w:name="Normal (Web)"/>
            <w:lsdException w:name="HTML Acronym"/>
            <w:lsdException w:name="HTML Address"/>
            <w:lsdException
                    w:name="HTML Cite"/>
            <w:lsdException w:name="HTML Code"/>
            <w:lsdException w:name="HTML Definition"/>
            <w:lsdException
                    w:name="HTML Keyboard"/>
            <w:lsdException w:name="HTML Preformatted"/>
            <w:lsdException w:name="HTML Sample"/>
            <w:lsdException
                    w:name="HTML Typewriter"/>
            <w:lsdException w:name="HTML Variable"/>
            <w:lsdException w:name="Normal Table"/>
            <w:lsdException
                    w:name="annotation subject"/>
            <w:lsdException w:name="Table Simple 1"/>
            <w:lsdException
                    w:name="Table Simple 2"/>
            <w:lsdException w:name="Table Simple 3"/>
            <w:lsdException w:name="Table Classic 1"/>
            <w:lsdException
                    w:name="Table Classic 2"/>
            <w:lsdException w:name="Table Classic 3"/>
            <w:lsdException
                    w:name="Table Classic 4"/>
            <w:lsdException w:name="Table Colorful 1"/>
            <w:lsdException
                    w:name="Table Colorful 2"/>
            <w:lsdException w:name="Table Colorful 3"/>
            <w:lsdException
                    w:name="Table Columns 1"/>
            <w:lsdException w:name="Table Columns 2"/>
            <w:lsdException
                    w:name="Table Columns 3"/>
            <w:lsdException w:name="Table Columns 4"/>
            <w:lsdException
                    w:name="Table Columns 5"/>
            <w:lsdException w:name="Table Grid 1"/>
            <w:lsdException w:name="Table Grid 2"/>
            <w:lsdException
                    w:name="Table Grid 3"/>
            <w:lsdException w:name="Table Grid 4"/>
            <w:lsdException w:name="Table Grid 5"/>
            <w:lsdException
                    w:name="Table Grid 6"/>
            <w:lsdException w:name="Table Grid 7"/>
            <w:lsdException w:name="Table Grid 8"/>
            <w:lsdException
                    w:name="Table List 1"/>
            <w:lsdException w:name="Table List 2"/>
            <w:lsdException w:name="Table List 3"/>
            <w:lsdException
                    w:name="Table List 4"/>
            <w:lsdException w:name="Table List 5"/>
            <w:lsdException w:name="Table List 6"/>
            <w:lsdException
                    w:name="Table List 7"/>
            <w:lsdException w:name="Table List 8"/>
            <w:lsdException w:name="Table 3D effects 1"/>
            <w:lsdException
                    w:name="Table 3D effects 2"/>
            <w:lsdException w:name="Table 3D effects 3"/>
            <w:lsdException
                    w:name="Table Contemporary"/>
            <w:lsdException w:name="Table Elegant"/>
            <w:lsdException
                    w:name="Table Professional"/>
            <w:lsdException w:name="Table Subtle 1"/>
            <w:lsdException
                    w:name="Table Subtle 2"/>
            <w:lsdException w:name="Table Web 1"/>
            <w:lsdException w:name="Table Web 2"/>
            <w:lsdException
                    w:name="Table Web 3"/>
            <w:lsdException w:name="Balloon Text"/>
            <w:lsdException w:name="Table Grid"/>
            <w:lsdException
                    w:name="Table Theme"/>
            <w:lsdException w:name="Light Shading"/>
            <w:lsdException w:name="Light List"/>
            <w:lsdException
                    w:name="Light Grid"/>
            <w:lsdException w:name="Medium Shading 1"/>
            <w:lsdException w:name="Medium Shading 2"/>
            <w:lsdException
                    w:name="Medium List 1"/>
            <w:lsdException w:name="Medium List 2"/>
            <w:lsdException w:name="Medium Grid 1"/>
            <w:lsdException
                    w:name="Medium Grid 2"/>
            <w:lsdException w:name="Medium Grid 3"/>
            <w:lsdException w:name="Dark List"/>
            <w:lsdException
                    w:name="Colorful Shading"/>
            <w:lsdException w:name="Colorful List"/>
            <w:lsdException w:name="Colorful Grid"/>
            <w:lsdException
                    w:name="Light Shading Accent 1"/>
            <w:lsdException w:name="Light List Accent 1"/>
            <w:lsdException
                    w:name="Light Grid Accent 1"/>
            <w:lsdException w:name="Medium Shading 1 Accent 1"/>
            <w:lsdException
                    w:name="Medium Shading 2 Accent 1"/>
            <w:lsdException w:name="Medium List 1 Accent 1"/>
            <w:lsdException
                    w:name="List Paragraph"/>
            <w:lsdException w:name="Medium List 2 Accent 1"/>
            <w:lsdException
                    w:name="Medium Grid 1 Accent 1"/>
            <w:lsdException w:name="Medium Grid 2 Accent 1"/>
            <w:lsdException
                    w:name="Medium Grid 3 Accent 1"/>
            <w:lsdException w:name="Dark List Accent 1"/>
            <w:lsdException
                    w:name="Colorful Shading Accent 1"/>
            <w:lsdException w:name="Colorful List Accent 1"/>
            <w:lsdException
                    w:name="Colorful Grid Accent 1"/>
            <w:lsdException w:name="Light Shading Accent 2"/>
            <w:lsdException
                    w:name="Light List Accent 2"/>
            <w:lsdException w:name="Light Grid Accent 2"/>
            <w:lsdException
                    w:name="Medium Shading 1 Accent 2"/>
            <w:lsdException w:name="Medium Shading 2 Accent 2"/>
            <w:lsdException
                    w:name="Medium List 1 Accent 2"/>
            <w:lsdException w:name="Medium List 2 Accent 2"/>
            <w:lsdException
                    w:name="Medium Grid 1 Accent 2"/>
            <w:lsdException w:name="Medium Grid 2 Accent 2"/>
            <w:lsdException
                    w:name="Medium Grid 3 Accent 2"/>
            <w:lsdException w:name="Dark List Accent 2"/>
            <w:lsdException
                    w:name="Colorful Shading Accent 2"/>
            <w:lsdException w:name="Colorful List Accent 2"/>
            <w:lsdException
                    w:name="Colorful Grid Accent 2"/>
            <w:lsdException w:name="Light Shading Accent 3"/>
            <w:lsdException
                    w:name="Light List Accent 3"/>
            <w:lsdException w:name="Light Grid Accent 3"/>
            <w:lsdException
                    w:name="Medium Shading 1 Accent 3"/>
            <w:lsdException w:name="Medium Shading 2 Accent 3"/>
            <w:lsdException
                    w:name="Medium List 1 Accent 3"/>
            <w:lsdException w:name="Medium List 2 Accent 3"/>
            <w:lsdException
                    w:name="Medium Grid 1 Accent 3"/>
            <w:lsdException w:name="Medium Grid 2 Accent 3"/>
            <w:lsdException
                    w:name="Medium Grid 3 Accent 3"/>
            <w:lsdException w:name="Dark List Accent 3"/>
            <w:lsdException
                    w:name="Colorful Shading Accent 3"/>
            <w:lsdException w:name="Colorful List Accent 3"/>
            <w:lsdException
                    w:name="Colorful Grid Accent 3"/>
            <w:lsdException w:name="Light Shading Accent 4"/>
            <w:lsdException
                    w:name="Light List Accent 4"/>
            <w:lsdException w:name="Light Grid Accent 4"/>
            <w:lsdException
                    w:name="Medium Shading 1 Accent 4"/>
            <w:lsdException w:name="Medium Shading 2 Accent 4"/>
            <w:lsdException
                    w:name="Medium List 1 Accent 4"/>
            <w:lsdException w:name="Medium List 2 Accent 4"/>
            <w:lsdException
                    w:name="Medium Grid 1 Accent 4"/>
            <w:lsdException w:name="Medium Grid 2 Accent 4"/>
            <w:lsdException
                    w:name="Medium Grid 3 Accent 4"/>
            <w:lsdException w:name="Dark List Accent 4"/>
            <w:lsdException
                    w:name="Colorful Shading Accent 4"/>
            <w:lsdException w:name="Colorful List Accent 4"/>
            <w:lsdException
                    w:name="Colorful Grid Accent 4"/>
            <w:lsdException w:name="Light Shading Accent 5"/>
            <w:lsdException
                    w:name="Light List Accent 5"/>
            <w:lsdException w:name="Light Grid Accent 5"/>
            <w:lsdException
                    w:name="Medium Shading 1 Accent 5"/>
            <w:lsdException w:name="Medium Shading 2 Accent 5"/>
            <w:lsdException
                    w:name="Medium List 1 Accent 5"/>
            <w:lsdException w:name="Medium List 2 Accent 5"/>
            <w:lsdException
                    w:name="Medium Grid 1 Accent 5"/>
            <w:lsdException w:name="Medium Grid 2 Accent 5"/>
            <w:lsdException
                    w:name="Medium Grid 3 Accent 5"/>
            <w:lsdException w:name="Dark List Accent 5"/>
            <w:lsdException
                    w:name="Colorful Shading Accent 5"/>
            <w:lsdException w:name="Colorful List Accent 5"/>
            <w:lsdException
                    w:name="Colorful Grid Accent 5"/>
            <w:lsdException w:name="Light Shading Accent 6"/>
            <w:lsdException
                    w:name="Light List Accent 6"/>
            <w:lsdException w:name="Light Grid Accent 6"/>
            <w:lsdException
                    w:name="Medium Shading 1 Accent 6"/>
            <w:lsdException w:name="Medium Shading 2 Accent 6"/>
            <w:lsdException
                    w:name="Medium List 1 Accent 6"/>
            <w:lsdException w:name="Medium List 2 Accent 6"/>
            <w:lsdException
                    w:name="Medium Grid 1 Accent 6"/>
            <w:lsdException w:name="Medium Grid 2 Accent 6"/>
            <w:lsdException
                    w:name="Medium Grid 3 Accent 6"/>
            <w:lsdException w:name="Dark List Accent 6"/>
            <w:lsdException
                    w:name="Colorful Shading Accent 6"/>
            <w:lsdException w:name="Colorful List Accent 6"/>
            <w:lsdException
                    w:name="Colorful Grid Accent 6"/>
        </w:latentStyles>
        <w:style w:type="paragraph" w:styleId="a1" w:default="on">
            <w:name w:val="Normal"/>
            <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
            </w:pPr>
            <w:rPr>
                <w:rFonts w:ascii="Calibri" w:h-ansi="Calibri" w:fareast="宋体" w:cs="Times New Roman" w:hint="default"/>
                <w:kern w:val="2"/>
                <w:sz w:val="21"/>
                <w:sz-cs w:val="22"/>
                <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:styleId="a7" w:default="on">
            <w:name w:val="Default Paragraph Font"/>
            <w:semiHidden/>
        </w:style>
        <w:style w:type="table" w:styleId="a6" w:default="on">
            <w:name w:val="Normal Table"/>
            <w:semiHidden/>
            <w:tblPr>
                <w:tblCellMar>
                    <w:top w:w="0" w:type="dxa"/>
                    <w:left w:w="108" w:type="dxa"/>
                    <w:bottom w:w="0"
                              w:type="dxa"/>
                    <w:right
                            w:w="108" w:type="dxa"/>
                </w:tblCellMar>
            </w:tblPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a2">
            <w:name w:val="Date"/>
            <w:basedOn w:val="a1"/>
            <w:next w:val="a1"/>
            <w:link w:val="a18"/>
            <w:semiHidden/>
            <w:pPr>
                <w:ind w:left="100" w:left-chars="2500"/>
            </w:pPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a3">
            <w:name w:val="Balloon Text"/>
            <w:basedOn w:val="a1"/>
            <w:link w:val="a15"/>
            <w:semiHidden/>
            <w:rPr>
                <w:sz w:val="18"/>
                <w:sz-cs w:val="18"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a4">
            <w:name w:val="footer"/>
            <w:basedOn w:val="a1"/>
            <w:link w:val="a10"/>
            <w:pPr>
                <w:tabs>
                    <w:tab w:val="center" w:pos="4153"/>
                    <w:tab w:val="right" w:pos="8306"/>
                </w:tabs>
                <w:snapToGrid w:val="off"/>
                <w:jc w:val="left"/>
            </w:pPr>
            <w:rPr>
                <w:sz w:val="18"/>
                <w:sz-cs w:val="18"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a5">
            <w:name w:val="header"/>
            <w:basedOn w:val="a1"/>
            <w:link w:val="a9"/>
            <w:pPr>
                <w:pBdr>
                    <w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="1" w:color="auto"/>
                </w:pBdr>
                <w:tabs>
                    <w:tab w:val="center" w:pos="4153"/>
                    <w:tab w:val="right" w:pos="8306"/>
                </w:tabs>
                <w:snapToGrid w:val="off"/>
                <w:jc w:val="center"/>
            </w:pPr>
            <w:rPr>
                <w:sz w:val="18"/>
                <w:sz-cs w:val="18"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:styleId="a8">
            <w:name w:val="Hyperlink"/>
            <w:basedOn w:val="a7"/>
            <w:rPr>
                <w:color w:val="0000FF"/>
                <w:u w:val="single"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:styleId="a9">
            <w:name w:val="页眉 字符"/>
            <w:basedOn w:val="a7"/>
            <w:link w:val="a5"/>
            <w:rPr>
                <w:sz w:val="18"/>
                <w:sz-cs w:val="18"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:styleId="a10">
            <w:name w:val="页脚 字符"/>
            <w:basedOn w:val="a7"/>
            <w:link w:val="a4"/>
            <w:rPr>
                <w:sz w:val="18"/>
                <w:sz-cs w:val="18"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a11">
            <w:name w:val="HTML Top of Form"/>
            <w:basedOn w:val="a1"/>
            <w:next w:val="a1"/>
            <w:link w:val="a12"/>
            <w:semiHidden/>
            <w:pPr>
                <w:widowControl/>
                <w:pBdr>
                    <w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="1" w:color="auto"/>
                </w:pBdr>
                <w:jc w:val="center"/>
            </w:pPr>
            <w:rPr>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:fareast="宋体" w:cs="Arial" w:hint="default"/>
                <w:vanish/>
                <w:kern w:val="0"/>
                <w:sz w:val="16"/>
                <w:sz-cs w:val="16"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:styleId="a12">
            <w:name w:val="z-窗体顶端 字符"/>
            <w:basedOn w:val="a7"/>
            <w:link w:val="a11"/>
            <w:semiHidden/>
            <w:rPr>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:fareast="宋体" w:cs="Arial" w:hint="default"/>
                <w:vanish/>
                <w:kern w:val="0"/>
                <w:sz w:val="16"/>
                <w:sz-cs w:val="16"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a13">
            <w:name w:val="HTML Bottom of Form"/>
            <w:basedOn w:val="a1"/>
            <w:next w:val="a1"/>
            <w:link w:val="a14"/>
            <w:pPr>
                <w:widowControl/>
                <w:pBdr>
                    <w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="1" w:color="auto"/>
                </w:pBdr>
                <w:jc w:val="center"/>
            </w:pPr>
            <w:rPr>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:fareast="宋体" w:cs="Arial" w:hint="default"/>
                <w:vanish/>
                <w:kern w:val="0"/>
                <w:sz w:val="16"/>
                <w:sz-cs w:val="16"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:styleId="a14">
            <w:name w:val="z-窗体底端 字符"/>
            <w:basedOn w:val="a7"/>
            <w:link w:val="a13"/>
            <w:rPr>
                <w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:fareast="宋体" w:cs="Arial" w:hint="default"/>
                <w:vanish/>
                <w:kern w:val="0"/>
                <w:sz w:val="16"/>
                <w:sz-cs w:val="16"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:styleId="a15">
            <w:name w:val="批注框文本 字符"/>
            <w:basedOn w:val="a7"/>
            <w:link w:val="a3"/>
            <w:semiHidden/>
            <w:rPr>
                <w:sz w:val="18"/>
                <w:sz-cs w:val="18"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a16">
            <w:name w:val="List Paragraph"/>
            <w:basedOn w:val="a1"/>
            <w:pPr>
                <w:ind w:first-line="420" w:first-line-chars="200"/>
            </w:pPr>
        </w:style>
        <w:style w:type="character" w:styleId="a17">
            <w:name w:val="Unresolved Mention"/>
            <w:basedOn w:val="a7"/>
            <w:semiHidden/>
            <w:rPr>
                <w:color w:val="605E5C"/>
                <w:shd w:val="clear" w:color="auto" w:fill="E1DFDD"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:styleId="a18">
            <w:name w:val="日期 字符"/>
            <w:basedOn w:val="a7"/>
            <w:link w:val="a2"/>
            <w:semiHidden/>
            <w:rPr/>
        </w:style>
    </w:styles>
    <w:bgPict>
        <w:background/>
        <v:background id="_x0000_s1025">
            <v:fill on="f" focussize="0,0"/>
        </v:background>
    </w:bgPict>
    <w:docPr>
        <w:view w:val="print"/>
        <w:zoom w:percent="120"/>
        <w:characterSpacingControl w:val="CompressPunctuation"/>
        <w:documentProtection w:enforcement="off"/>
        <w:punctuationKerning/>
        <w:doNotEmbedSystemFonts/>
        <w:bordersDontSurroundHeader/>
        <w:bordersDontSurroundFooter/>
        <w:defaultTabStop w:val="420"/>
        <w:drawingGridVerticalSpacing w:val="156"/>
        <w:displayHorizontalDrawingGridEvery w:val="0"/>
        <w:displayVerticalDrawingGridEvery w:val="2"/>
        <w:compat>
            <w:adjustLineHeightInTable/>
            <w:ulTrailSpace/>
            <w:doNotExpandShiftReturn/>
            <w:balanceSingleByteDoubleByteWidth/>
            <w:useFELayout/>
            <w:spaceForUL/>
            <w:wrapTextWithPunct/>
            <w:breakWrappedTables/>
            <w:useAsianBreakRules/>
            <w:dontGrowAutofit/>
            <w:useFELayout/>
        </w:compat>
    </w:docPr>
    <w:body>
        <wx:sect>
            <w:p>
                <w:pPr>
                    <w:jc w:val="distribute"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:b/>
                        <w:color w:val="0070C0"/>
                        <w:sz w:val="72"/>
                        <w:u w:val="thick"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:color
                                w:val="0070C0"/>
                        <w:sz w:val="72"/>
                        <w:u w:val="thick"/>
                        <w:lang w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t>${data.header.reportName!""}</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:color
                                w:val="0070C0"/>
                        <w:sz w:val="72"/>
                        <w:u w:val="thick"/>
                    </w:rPr>
                    <w:t></w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:b/>
                        <w:color
                                w:val="0070C0"/>
                        <w:sz w:val="72"/>
                        <w:u w:val="thick"/>
                    </w:rPr>
                    <w:t></w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:jc w:val="center"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="72"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:jc w:val="center"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="72"/>
                        <w:lang w:fareast="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:color
                                w:val="000000"/>
                        <w:sz w:val="72"/>
                        <w:lang w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t>${data.header.buyerName!""}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:jc w:val="center"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:b/>
                        <w:color w:val="244061"/>
                        <w:sz w:val="56"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:color
                                w:val="244061"/>
                        <w:sz w:val="56"/>
                    </w:rPr>
                    <w:t>合</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:jc w:val="center"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:b/>
                        <w:color w:val="244061"/>
                        <w:sz w:val="56"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:color
                                w:val="244061"/>
                        <w:sz w:val="56"/>
                    </w:rPr>
                    <w:t>同</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:jc w:val="center"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:b/>
                        <w:color w:val="244061"/>
                        <w:sz w:val="56"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:color
                                w:val="244061"/>
                        <w:sz w:val="56"/>
                    </w:rPr>
                    <w:t>体</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:jc w:val="center"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:b/>
                        <w:color w:val="244061"/>
                        <w:sz w:val="56"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:color
                                w:val="244061"/>
                        <w:sz w:val="56"/>
                    </w:rPr>
                    <w:t>检</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:jc w:val="center"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:b/>
                        <w:color w:val="244061"/>
                        <w:sz w:val="56"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:color
                                w:val="244061"/>
                        <w:sz w:val="56"/>
                    </w:rPr>
                    <w:t>报</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:jc w:val="center"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:b/>
                        <w:color w:val="244061"/>
                        <w:sz w:val="56"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:color
                                w:val="244061"/>
                        <w:sz w:val="56"/>
                    </w:rPr>
                    <w:t>告</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:jc w:val="center"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:b/>
                        <w:color w:val="244061"/>
                        <w:sz w:val="56"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:after-autospacing="off"/>
                    <w:jc w:val="center"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:color w:val="244061"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="10"/>
                        <w:lang w:fareast="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:color
                                w:val="244061"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="10"/>
                        <w:lang w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t>${data.header.address!""}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:before="159" w:before-lines="51" w:before-autospacing="off"/>
                    <w:jc
                            w:val="center"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:color w:val="244061"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="10"/>
                        <w:lang w:fareast="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:color
                                w:val="244061"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="10"/>
                        <w:lang w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t>${data.header.YMD!""}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:color
                                w:val="244061"/>
                        <w:sz w:val="28"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:color
                                w:val="244061"/>
                        <w:sz w:val="28"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:rPr>
                        <w:b/>
                        <w:color w:val="0070C0"/>
                        <w:sz
                                w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="黑体" w:h-ansi="黑体" w:fareast="黑体" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="36"/>
                        <w:u
                                w:val="thick"/>
                        <w:lang w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t>${data.body.buyerName!""}</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:hint="fareast"/>
                        <w:b/>
                        <w:color w:val="0070C0"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                    <w:t>：</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN w:val="off"/>
                    <w:adjustRightInd
                            w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind w:first-line="562"
                           w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="黑体" w:h-ansi="黑体" w:fareast="黑体" w:hint="fareast"/>
                        <w:b/>
                        <w:b-cs/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="黑体" w:h-ansi="黑体" w:fareast="黑体" w:hint="fareast"/>
                        <w:b/>
                        <w:b-cs/>
                        <w:sz
                                w:val="28"/>
                        <w:sz-cs w:val="28"/>
                    </w:rPr>
                    <w:t>根据您选答的结果，通过法律、中律法检风险数据库、相关大数据，利用人工智能进行数据统计、分析、判断、评价、评估，向您出具体检报告。</w:t>
                </w:r>
            </w:p>
 <#list data.body.points as points>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN w:val="off"/>
                    <w:adjustRightInd
                            w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind w:first-line="482"
                           w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:b-cs/>
                        <w:color w:val="002060"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:autoSpaceDE w:val="off"/>
                        <w:autoSpaceDN w:val="off"/>
                        <w:adjustRightInd
                                w:val="off"/>
                        <w:spacing w:line="360" w:line-rule="auto"/>
                        <w:ind w:first-line="482"
                               w:first-line-chars="200"/>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:b-cs/>
                        <w:color
                                w:val="002060"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                        <w:t>${points.pointName!""} </w:t>
                    </w:rPr>

                </w:r>
            </w:p>
 <#list points.pointList as plist>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN w:val="off"/>
                    <w:adjustRightInd
                            w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind w:first-line="482"
                           w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:b/>

                        <w:b-cs/>
                        <w:color w:val="C00000"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr></w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="24"/>
                        <w:sz-cs
                                w:val="24"/>
                    </w:rPr>
                    <w:t>【风险点 ${plist.riskIndex!"无"}】</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN
                            w:val="off"/>
                    <w:adjustRightInd w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind
                            w:first-line="480" w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:b/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                        <w:lang
                                w:val="ZH-CN"/>
                    </w:rPr>
                    <w:t>${plist.riskInedexContent!"无"}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN
                            w:val="off"/>
                    <w:adjustRightInd w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind
                            w:first-line="482" w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:b/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="24"/>
                        <w:sz-cs
                                w:val="24"/>
                    </w:rPr>
                    <w:t>【风险等级】</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN w:val="off"/>
                    <w:adjustRightInd
                            w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind w:first-line="480"
                           w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:b/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                        <w:lang
                                w:val="ZH-CN"/>
                    </w:rPr>
                    <w:t>${plist.riskLevel!"无"}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN
                            w:val="off"/>
                    <w:adjustRightInd w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind
                            w:first-line="482" w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:b/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="24"/>
                        <w:sz-cs
                                w:val="24"/>
                    </w:rPr>
                    <w:t>【危害后果】</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN w:val="off"/>
                    <w:adjustRightInd
                            w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind w:first-line="480"
                           w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                        <w:lang w:val="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                        <w:lang
                                w:val="ZH-CN"/>
                    </w:rPr>
                    <w:t>
                        ${plist.riskResult!"无"}
                    </w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN
                            w:val="off"/>
                    <w:adjustRightInd w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind
                            w:first-line="482" w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:b/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="24"/>
                        <w:sz-cs
                                w:val="24"/>
                    </w:rPr>
                    <w:t>【法律依据】</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind
                            w:first-line="480" w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:b/>
                        <w:b-cs/>
                        <w:color w:val="C00000"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                        <w:lang w:val="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                        <w:lang
                                w:val="ZH-CN"/>
                    </w:rPr>
                    <w:t>${plist.riskLaw!"无"}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind
                            w:first-line="480" w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                        <w:lang w:val="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN
                            w:val="off"/>
                    <w:adjustRightInd w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind
                            w:first-line="482" w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="24"/>
                        <w:sz-cs
                                w:val="24"/>
                    </w:rPr>
                    <w:t>【管控措施】</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN w:val="off"/>
                    <w:adjustRightInd
                            w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind w:first-line="480"
                           w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                        <w:lang w:val="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                    <w:t>${plist.riskControl!"无"}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN w:val="off"/>
                    <w:adjustRightInd
                            w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind w:first-line="402"
                           w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>


                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                        <w:lang w:val="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
    </#list>

</#list>
            <w:p>
                <w:pPr>
                    <w:jc w:val="center"/>
                    <w:rPr>
                        <w:b/>
                        <w:color w:val="1F497D"/>
                        <w:sz w:val="44"/>
                        <w:sz-cs w:val="44"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:hint="fareast"/>
                        <w:b/>
                        <w:color w:val="1F497D"/>
                        <w:sz w:val="44"/>
                        <w:sz-cs w:val="44"/>
                    </w:rPr>
                    <w:t>【</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:hint="fareast"/>
                        <w:b/>
                        <w:color w:val="1F497D"/>
                        <w:sz w:val="44"/>
                        <w:sz-cs w:val="44"/>
                        <w:lang
                                w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t>综合结论</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:hint="fareast"/>
                        <w:b/>
                        <w:color w:val="1F497D"/>
                        <w:sz w:val="44"/>
                        <w:sz-cs w:val="44"/>
                    </w:rPr>
                    <w:t>】</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:ind w:first-line="562" w:first-line-chars="200"/>
                    <w:rPr>
                        <w:b/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:hint="fareast"/>
                        <w:b/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                        <w:lang
                                w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t>${data.conclusion!"无"}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:rPr>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:jc w:val="center"/>
                    <w:rPr>
                        <w:rFonts w:hint="fareast"/>
                        <w:b/>
                        <w:color w:val="1F497D"/>
                        <w:sz w:val="44"/>
                        <w:sz-cs w:val="44"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:hint="fareast"/>
                        <w:b/>
                        <w:color w:val="1F497D"/>
                        <w:sz w:val="44"/>
                        <w:sz-cs w:val="44"/>
                    </w:rPr>
                    <w:t>【解决方案】</w:t>
                </w:r>
            </w:p>
            <#if data.programs??>
            <#list data.programs as program>
            <w:p>
                <w:pPr>
                    <w:ind w:first-line="562" w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:fareast="宋体" w:hint="default"/>
                        <w:b/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:fareast="宋体" w:hint="fareast"/>
                        <w:b/>
                        <w:color
                                w:val="000000"/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                    </w:rPr>
                    <w:t>${program.programIndex!"无"}</w:t>
                </w:r>
            </w:p>
            <w:p>

                <w:pPr>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:fareast="宋体" w:hint="default"/>
                        <w:b/>
                        <w:color
                                w:val="000000"/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:fareast="宋体" w:hint="fareast"/>
                        <w:b/>
                        <w:color
                                w:val="000000"/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                    </w:rPr>
                    <w:t>${program.programContent!"无"}</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:fareast="宋体" w:hint="default"/>
                        <w:b/>
                        <w:color
                                w:val="000000"/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                    </w:rPr>
                    <w:t></w:t>
                </w:r>
                <w:r>
                    <w:fldChar w:fldCharType="begin"/>
                </w:r>
                <w:r>
                    <w:instrText> HYPERLINK ${program.programUrl!"无"}</w:instrText>
                </w:r>
                <w:r>
                    <w:fldChar w:fldCharType="separate"/>
                </w:r>
            </w:p>
                </#list>
                <#else>
                    <w:p>
                        <w:pPr>
                            <w:ind w:first-line="562" w:first-line-chars="200"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:fareast="宋体" w:hint="default"/>
                                <w:b/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="28"/>
                                <w:sz-cs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:fareast="宋体" w:hint="fareast"/>
                                <w:b/>
                                <w:color
                                        w:val="000000"/>
                                <w:sz w:val="28"/>
                                <w:sz-cs w:val="28"/>
                            </w:rPr>
                            <w:t>无</w:t>
                        </w:r>
                    </w:p>
                </#if>


            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN w:val="off"/>
                    <w:adjustRightInd
                            w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN w:val="off"/>
                    <w:adjustRightInd
                            w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind w:first-line="480"
                           w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs
                                w:val="24"/>
                    </w:rPr>
                    <w:t>本报告是在默认您选答问题的基础上进行的分析、论证，对您所选答问题的真实性、客观性、全面性不进行审查，本报告不能保证得出唯一、准确的结论，出具报告</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs
                                w:val="24"/>
                    </w:rPr>
                    <w:t>10</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs
                                w:val="24"/>
                    </w:rPr>
                    <w:t>日报告意见仅供您参考。</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN w:val="off"/>
                    <w:adjustRightInd
                            w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind w:first-line="480"
                           w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:color w:val="C00000"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs
                                w:val="24"/>
                    </w:rPr>
                    <w:t>体检报告知识产权属于中律法检所有。仅限自己管控风险参考使用，不得用于其他任何目的，不得外泄、出版、发行及其他侵权行为。</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN
                            w:val="off"/>
                    <w:adjustRightInd w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind
                            w:first-line="480" w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:color w:val="C00000"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:autoSpaceDE w:val="off"/>
                    <w:autoSpaceDN w:val="off"/>
                    <w:adjustRightInd
                            w:val="off"/>
                    <w:spacing w:line="360" w:line-rule="auto"/>
                    <w:ind w:first-line="480"
                           w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="default"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="微软雅黑" w:h-ansi="微软雅黑" w:fareast="微软雅黑" w:hint="fareast"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs
                                w:val="24"/>
                    </w:rPr>
                    <w:t>体检报告，仅供参考，不作为法律依据！</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:ind w:first-line="480"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:fareast="宋体" w:hint="default"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:ind w:first-line="480"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:fareast="宋体" w:hint="default"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:ind w:first-line="480"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:fareast="宋体" w:hint="default"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:ind w:first-line="480"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:fareast="宋体" w:hint="default"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:fareast="宋体" w:hint="default"/>
                        <w:b/>
                        <w:color
                                w:val="C00000"/>
                        <w:sz w:val="24"/>
                        <w:sz-cs w:val="24"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:jc w:val="right"/>
                    <w:rPr>
                        <w:rFonts w:ascii="黑体" w:h-ansi="黑体" w:fareast="黑体" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                        <w:lang w:fareast="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="黑体" w:h-ansi="黑体" w:fareast="黑体" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="32"/>
                        <w:sz-cs
                                w:val="32"/>
                    </w:rPr>
                    <w:t>体检机构：</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="黑体" w:h-ansi="黑体" w:fareast="黑体" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="32"/>
                        <w:sz-cs
                                w:val="32"/>
                        <w:lang w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t>${data.pageFooter.institutionName!"无"}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:rPr>
                        <w:rFonts w:ascii="黑体" w:h-ansi="黑体" w:fareast="黑体" w:hint="default"/>
                        <w:b/>
                        <w:color
                                w:val="C00000"/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="黑体" w:h-ansi="黑体" w:fareast="黑体" w:hint="fareast"/>
                        <w:b/>
                        <w:color
                                w:val="C00000"/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                    </w:rPr>
                    <w:t></w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:jc w:val="right"/>
                    <w:rPr>
                        <w:rFonts w:ascii="黑体" w:h-ansi="黑体" w:fareast="黑体" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                        <w:lang w:fareast="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="黑体" w:h-ansi="黑体" w:fareast="黑体" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="32"/>
                        <w:sz-cs
                                w:val="32"/>
                        <w:lang w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t>${data.pageFooter.YMD!"无"}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="500" w:line-rule="exact"/>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="500" w:line-rule="exact"/>
                    <w:ind
                            w:first-line="585"/>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                        <w:lang w:fareast="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs
                                w:val="28"/>
                    </w:rPr>
                    <w:t>【官方网站】</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs
                                w:val="28"/>
                        <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t> ${data.pageFooter.website!"无"}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="500" w:line-rule="exact"/>
                    <w:ind
                            w:first-line="585"/>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="default"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                        <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs
                                w:val="28"/>
                    </w:rPr>
                    <w:t>【公众号】</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs
                                w:val="28"/>
                        <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t> ${data.pageFooter.publicNumber!"无"}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="500" w:line-rule="exact"/>
                    <w:ind
                            w:first-line="585"/>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="default"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                        <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs
                                w:val="28"/>
                    </w:rPr>
                    <w:t>【A</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="default"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs
                                w:val="28"/>
                    </w:rPr>
                    <w:t>PP</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs
                                w:val="28"/>
                    </w:rPr>
                    <w:t>下载】</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs
                                w:val="28"/>
                        <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t> ${data.pageFooter.APPDownload!"无"}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="500" w:line-rule="exact"/>
                    <w:ind
                            w:first-line="585"/>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="default"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                        <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs
                                w:val="28"/>
                    </w:rPr>
                    <w:t>【邮箱】</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs
                                w:val="28"/>
                        <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t> ${data.pageFooter.emial!"无"}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="500" w:line-rule="exact"/>
                    <w:ind
                            w:first-line="562" w:first-line-chars="200"/>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs w:val="28"/>
                        <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs
                                w:val="28"/>
                    </w:rPr>
                    <w:t>【地址】</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs
                                w:val="28"/>
                        <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t></w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs
                                w:val="28"/>
                        <w:lang w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t>${data.pageFooter.address!"无"}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="500" w:line-rule="exact"/>
                    <w:ind
                            w:first-line="585"/>
                    <w:rPr>
                        <w:rFonts w:ascii="黑体" w:h-ansi="黑体" w:fareast="仿宋_GB2312" w:hint="default"/>
                        <w:b/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                        <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs
                                w:val="28"/>
                    </w:rPr>
                    <w:t>【编码】</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="28"/>
                        <w:sz-cs
                                w:val="28"/>
                        <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                    </w:rPr>
                    <w:t> ${data.pageFooter.code!"无"}</w:t>
                </w:r>
            </w:p>
            <w:sectPr>
                <w:hdr w:type="odd">
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="a5"/>
                            <w:tabs>
                                <w:tab w:val="left" w:pos="2580"/>
                                <w:tab w:val="left" w:pos="2985"/>
                            </w:tabs>
                            <w:wordWrap w:val="off"/>
                            <w:spacing w:after="120" w:line="276"
                                       w:line-rule="auto"/>
                            <w:jc
                                    w:val="right"/>
                            <w:rPr>
                                <w:rFonts w:ascii="黑体" w:h-ansi="黑体" w:fareast="黑体" w:hint="default"/>
                                <w:color w:val="31849B"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="Cambria" w:h-ansi="Cambria" w:fareast="宋体" w:cs="Times New Roman"
                                          w:hint="default"/>
                                <w:b/>
                                <w:b-cs/>
                                <w:color
                                        w:val="4F81BD"/>
                                <w:sz w:val="24"/>
                                <w:sz-cs w:val="24"/>
                            </w:rPr>
                            <w:tab/>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="黑体" w:h-ansi="黑体" w:fareast="黑体" w:hint="fareast"/>
                                <w:color
                                        w:val="31849B"/>
                                <w:lang w:fareast="ZH-CN"/>
                            </w:rPr>
                            <w:t>${data.pageHeader.institutionName!"无"}</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="黑体" w:h-ansi="黑体" w:fareast="黑体" w:hint="fareast"/>
                                <w:color
                                        w:val="31849B"/>
                            </w:rPr>
                            <w:t></w:t>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="a5"/>
                            <w:tabs>
                                <w:tab w:val="left" w:pos="2580"/>
                                <w:tab w:val="left" w:pos="2985"/>
                            </w:tabs>
                            <w:spacing w:after="120" w:line="276" w:line-rule="auto"/>
                            <w:jc
                                    w:val="right"/>
                            <w:rPr>
                                <w:rFonts w:fareast="宋体" w:hint="fareast"/>
                                <w:color w:val="4F81BD"/>
                                <w:lang w:fareast="ZH-CN"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="黑体" w:h-ansi="黑体" w:fareast="黑体" w:hint="fareast"/>
                                <w:color w:val="31849B"/>
                                <w:lang
                                        w:fareast="ZH-CN"/>
                            </w:rPr>
                            <w:t>${data.pageHeader.title!"无"}</w:t>
                        </w:r>
                    </w:p>
                </w:hdr>
                <w:pgSz w:w="11906" w:h="16838"/>
                <w:pgMar w:top="851" w:right="1701" w:bottom="851" w:left="1797" w:header="851" w:footer="992"
                         w:gutter="0"/>
                <w:cols w:space="425"/>
                <w:docGrid w:type="lines" w:line-pitch="312"/>
            </w:sectPr>
           </w:p>
        </wx:sect>
    </w:body></w:wordDocument>