package com.mumu233.test;

import java.util.*;

import com.alibaba.fastjson.JSON;
import com.mumu233.test.mapper.ProductInfoMapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestApplicationTests {

    @Autowired
    private ProductInfoMapper productInfoMapper;
    @Test
    public void contextLoads() {

        ReportFTLEntity value = new ReportFTLEntity();

        ReportFTLEntity.Header header = new ReportFTLEntity.Header();
        header.setReportName("中律法检体检报告");
        header.setAddress("中国 北京");
        header.setBuyerName("叶枫");
        header.setYMD("2019年12月14日");

        value.setHeader(header);



        ReportFTLEntity.PageHeader pageHeader = new ReportFTLEntity.PageHeader();
        pageHeader.setInstitutionName("中律法检");
        pageHeader.setTitle("查找风险 评估风险 防控风险");

        value.setPageHeader(pageHeader);
        value.setConclusion("测试综合风险");

        ReportFTLEntity.PageFooter pageFooter = new ReportFTLEntity.PageFooter();
        pageFooter.setInstitutionName("中律法检");
        pageFooter.setYMD("2019年12月14日");
        pageFooter.setAddress("中国 北京");
        pageFooter.setAPPDownload("http://www.baidu.com");
        pageFooter.setWebsite("http://www.baidu.com");
        pageFooter.setCode("552100");
        pageFooter.setEmial("599119084@qq.com");
        pageFooter.setPublicNumber("123456789");

        value.setPageFooter(pageFooter);


        List<ReportFTLEntity.Body.Points> points = new ArrayList<>();

        for (int i = 0; i < 2; i++) {
            ReportFTLEntity.Body.Points tmp = new ReportFTLEntity.Body.Points();
            tmp.setPointName("测试板块数据" + (i + 1));

            List<ReportFTLEntity.Body.Points.PointList> pointLists = new ArrayList<>();

            for (int i1 = 0; i1 < 2; i1++) {
                ReportFTLEntity.Body.Points.PointList tmp2 = new ReportFTLEntity.Body.Points.PointList();
                tmp2.setRiskIndex(i+1);
                tmp2.setRiskInedexContent("测试风险点数据" + (i1+1));
                tmp2.setRiskLaw("测试，太懒了不写了");
                pointLists.add(tmp2);
            }

            tmp.setPointList(pointLists);
            points.add(tmp);
        }
        ReportFTLEntity.Body body = new ReportFTLEntity.Body();
        body.setBuyerName("叶枫");
        body.setPoints(points);

        value.setBody(body);



        Map data = new HashMap();
        data.put("data",value);
        Configuration configuration = new Configuration();
        configuration.setClassForTemplateLoading(this.getClass(),"/templates");
        configuration.setDefaultEncoding("utf-8");

        Template template = null;

        try {
            template = configuration.getTemplate("template.ftl");
        } catch (IOException e) {
            e.printStackTrace();
        }

        File outFile = new File("/home/yefeng/java/out/out.doc");
        Writer out = null;

        try {
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "utf-8"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            assert template != null;
            template.process(data, out);
        } catch (TemplateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }
    @Test
    public void changeTest(){
        List<ProductInfoEntity> finList = productInfoMapper.selectAll();

        AtomicReference<Integer> cout = new AtomicReference<>(0);

        finList.forEach(tmp ->{

            ProductInfoEntity update = new ProductInfoEntity();
            List<ProductSection> params = JSON.parseArray(tmp.getProductSection(),ProductSection.class);
            params.forEach(param -> {
                param.setDocUrl("");
            });
            update.setProductSection(JSON.toJSONString(params));
            update.setId(tmp.getId());
            cout.set(cout.get() + 1);
            productInfoMapper.updateByPrimaryKeySelective(update);
        });

        System.out.println(cout.get());
    }
    @Test
    public void findOne(){

//        Example example = Example.builder(ProductInfoEntity.class)
//                .select("cout(*)")
//                .where(Sqls.custom().andEqualTo("id",100))
//                .build();


    }

}
